content = {}
content.exposureAlpha = 0
content.destExposureAlpha = 0

local function updateDestAlpha()
    content.destExposureAlpha = math.random(255)
end

local function updateExposure(dt)
    content.exposureAlpha = lerp(content.exposureAlpha, content.destExposureAlpha, 5 * dt)
    if math.abs(content.exposureAlpha - content.destExposureAlpha) < 20 then
        updateDestAlpha()
    end
end

function game.load()
    content.observatory = love.graphics.newImage("res/levels/observatory.jpg")
    content.observatoryLight = love.graphics.newImage("res/levels/observatory_light.png")
    content.observatoryExpose = love.graphics.newImage("res/levels/observatory_exposed.png")
    content.lightOn = false
    content.timeout = love.timer.getTime()
end
function game.update(dt)
    if gui.Button{text="right-leave", pos={0.78 * love.graphics.getWidth(), 80}, size={0.25 * love.graphics.getWidth(), love.graphics.getHeight()}} then
        game.switchLevelFade("Hall")
    end

    if gui.Button{text="scientist", pos={0.41 * love.graphics.getWidth(), 0.64 * love.graphics.getHeight()}, size={ 0.16 *love.graphics.getWidth(), 0.12 * love.graphics.getHeight()}} then
        if not game.state.gotScientistKey then
            inventory.addItem("lockerKey")
            game.state.gotScientistKey = true
        else
            messageManager.setMessage(game.storyboard.deadScientist)
        end
    end

    if gui.Button{text="panel", pos={0.35 * love.graphics.getWidth(), 0.52 * love.graphics.getHeight()}, size={ 0.06 *love.graphics.getWidth(), 0.06 * love.graphics.getHeight()}} then
        if not game.state.hallDoorOpened then
            game.switchLevelFade("Panel")
        else
            messageManager.setMessage(game.storyboard.nothingThere)
        end
    end

    if love.timer.getTime() - content.timeout > 0.5 then
        content.timeout = love.timer.getTime()
        content.lightOn = not content.lightOn
    end

    updateExposure(dt)
end
function game.draw()
    love.graphics.draw(content.observatory, game.shared.xOffset, game.shared.yOffset, 0,
        game.shared.scale,
        game.shared.scale)

    if content.lightOn then
        love.graphics.draw(content.observatoryLight, game.shared.xOffset, game.shared.yOffset, 0,
            game.shared.scale,
            game.shared.scale)
    end

    local colorBak = {love.graphics.getColor()}
    love.graphics.setColor({255, 255, 255, content.exposureAlpha})
    love.graphics.draw(content.observatoryExpose, game.shared.xOffset, game.shared.yOffset, 0,
        game.shared.scale,
        game.shared.scale)
    love.graphics.setColor(colorBak)

end
