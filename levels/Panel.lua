content = {}

local Button = {}
Button.__index = Button
local combination = {true, false, false, true, false, true, false}

function Button:update(dt)
    dt = dt * 2
    if self.targetState then
        if self.rotation > self.rotationOn then
            self.rotation = self.rotation - dt
        end
        if self.rotation <= self.rotationOn then
            self.rotation = self.rotationOn
        end
    else
        if self.rotation < self.rotationOff then
            self.rotation = self.rotation + dt
        end
        if self.rotation >= self.rotationOff then
            self.rotation = self.rotationOff
        end
    end
    if self.rotation == self.rotationOn then
        self.state = true
    end
    if self.rotation == self.rotationOff then
        self.state = false
    end
end
function Button:draw()
    love.graphics.draw(content.panelButton,
        self.posx,
        self.posy,
        self.rotation,
        game.shared.scale,
        game.shared.scale,
        content.panelButton:getWidth() / 2,
        content.panelButton:getHeight() / 2
    )
    if self.state then
        love.graphics.draw(content.panelGreen,
            self.posx + 0.06 * love.graphics.getWidth(),
            self.posy + 0.2 * love.graphics.getHeight(),
            0,
            game.shared.scale,
            game.shared.scale,
            content.panelGreen:getWidth() / 2,
            content.panelGreen:getHeight() / 2)
    else
        love.graphics.draw(content.panelRed,
            self.posx - 0.06 * love.graphics.getWidth(),
            self.posy + 0.2 * love.graphics.getHeight(),
            0,
            game.shared.scale,
            game.shared.scale,
            content.panelGreen:getWidth() / 2,
            content.panelGreen:getHeight() / 2)
    end
end
function Button:startToggle()
    self.targetState = not self.targetState
end
function Button.create(posx, posy)
    local button = {}
    setmetatable(button, Button)
    button.state = false
    button.rotation = 0
    button.rotationOn = -0.3 * math.pi
    button.posx = posx
    button.posy = posy
    button.rotationOff = 0
    button.targetState = false
    return button;
end

local function buttons()
    if gui.Button{text="button1", pos={0.1 * love.graphics.getWidth(), 0.165 * love.graphics.getHeight()}, size={ 0.13 *love.graphics.getWidth(), 0.20 * love.graphics.getHeight()}} then
        game.state.panelState[1]:startToggle()
    end
    if gui.Button{text="button2", pos={0.34 * love.graphics.getWidth(), 0.165 * love.graphics.getHeight()}, size={ 0.13 *love.graphics.getWidth(), 0.20 * love.graphics.getHeight()}} then
        game.state.panelState[2]:startToggle()
    end
    if gui.Button{text="button3", pos={0.57 * love.graphics.getWidth(), 0.165 * love.graphics.getHeight()}, size={ 0.13 *love.graphics.getWidth(), 0.20 * love.graphics.getHeight()}} then
        game.state.panelState[3]:startToggle()
    end
    if gui.Button{text="button4", pos={0.80 * love.graphics.getWidth(), 0.165 * love.graphics.getHeight()}, size={ 0.13 *love.graphics.getWidth(), 0.20 * love.graphics.getHeight()}} then
        game.state.panelState[4]:startToggle()
    end
    if gui.Button{text="button5", pos={0.22 * love.graphics.getWidth(), 0.6 * love.graphics.getHeight()}, size={ 0.13 *love.graphics.getWidth(), 0.20 * love.graphics.getHeight()}} then
        game.state.panelState[5]:startToggle()
    end
    if gui.Button{text="button6", pos={0.45 * love.graphics.getWidth(), 0.6 * love.graphics.getHeight()}, size={ 0.13 *love.graphics.getWidth(), 0.20 * love.graphics.getHeight()}} then
        game.state.panelState[6]:startToggle()
    end
    if gui.Button{text="button7", pos={0.69 * love.graphics.getWidth(), 0.6 * love.graphics.getHeight()}, size={ 0.13 *love.graphics.getWidth(), 0.20 * love.graphics.getHeight()}} then
        game.state.panelState[7]:startToggle()
    end
end

function game.load()
    content.lockersClosed = love.graphics.newImage("res/levels/panel.jpg")
    content.panelLight = love.graphics.newImage("res/levels/panel_light.png")
    content.panelButton = love.graphics.newImage("res/sprites/panel_button.png")
    content.panelRed = love.graphics.newImage("res/sprites/panel_red.png")
    content.panelGreen = love.graphics.newImage("res/sprites/panel_green.png")
    if not game.state.panelState then
        game.state.panelState = {
            Button.create(0.16 * love.graphics.getWidth(), 0.27 * love.graphics.getHeight()), 
            Button.create(0.396 * love.graphics.getWidth(), 0.27 * love.graphics.getHeight()), 
            Button.create(0.626 * love.graphics.getWidth(), 0.27 * love.graphics.getHeight()), 
            Button.create(0.862 * love.graphics.getWidth(), 0.27 * love.graphics.getHeight()), 
            Button.create(0.280 * love.graphics.getWidth(), 0.696 * love.graphics.getHeight()), 
            Button.create(0.51 * love.graphics.getWidth(), 0.696 * love.graphics.getHeight()), 
            Button.create(0.74 * love.graphics.getWidth(), 0.696 * love.graphics.getHeight()), 
        }
        io.write(game.state.panelState[2].rotation);
    end

    content.lightOn = false
    content.timeout = love.timer.getTime()

end
function game.update(dt)
    buttons()

    if gui.Button{text="left-leave", pos={0, 0}, size={0.06 * love.graphics.getWidth(), love.graphics.getHeight()}} then
        game.switchLevelFade("Observatory")
    end
    if gui.Button{text="right-leave", pos={0.96 * love.graphics.getWidth(), 0}, size={0.04 * love.graphics.getWidth(), love.graphics.getHeight()}} then
        game.switchLevelFade("Observatory")
    end
    if gui.Button{text="bottom-leave", pos={0, 0.90 * love.graphics.getHeight()}, size={love.graphics.getWidth(), 0.10 * love.graphics.getHeight()}} then
        game.switchLevelFade("Observatory")
    end

    if love.timer.getTime() - content.timeout > 0.5 then
        content.timeout = love.timer.getTime()
        content.lightOn = not content.lightOn
    end

    local combinationCorrect = true
    for i,v in ipairs(game.state.panelState) do
        v:update(dt)
        if v.state ~= combination[i] then
            combinationCorrect = false
        end
    end
    if combinationCorrect then
        game.switchLevelFade("Observatory")
        game.state.hallDoorOpened = true
    end

end


function game.draw()
    love.graphics.draw(content.lockersClosed, game.shared.xOffset, game.shared.yOffset, 0,
        game.shared.scale,
        game.shared.scale)
    if content.lightOn then
        love.graphics.draw(content.panelLight, game.shared.xOffset, game.shared.yOffset, 0,
            game.shared.scale,
            game.shared.scale)
    end

    drawPanelButtons()
end


function drawPanelButtons()
    for _, v in ipairs(game.state.panelState) do
        v:draw()
    end
end