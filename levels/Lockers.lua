content = {}
local state = "closed"
local timeout = 0
function game.load()
    content.lockersClosed = love.graphics.newImage("res/levels/lockers_closed.jpg")
end
function game.update()
    if state == "openSuitless" or state == "closedSuitless" then
        if(love.timer.getTime() - timeout > 1) then
            if state == "openSuitless" then
                state = "closedSuitless"
                timeout = love.timer.getTime()
            else
                game.switchLevelFade("Storage")
            end
        end
        return
    end
    if gui.Button{text="left-leave", pos={0, 80}, size={0.15 * love.graphics.getWidth(), love.graphics.getHeight()}} then
        game.switchLevelFade("Storage")
    end
    if gui.Button{text="right-leave", pos={0.85 * love.graphics.getWidth(), 80}, size={0.15 * love.graphics.getWidth(), love.graphics.getHeight()}} then
        game.switchLevelFade("Storage")
    end
    if gui.Button{text="bottom-leave", pos={0, 0.90 * love.graphics.getHeight()}, size={love.graphics.getWidth(), 0.10 * love.graphics.getHeight()}} then
        game.switchLevelFade("Storage")
    end
    if state ~= "openSuitless" then
        if gui.Button{text="locker", pos={0.5 * love.graphics.getWidth(), 0.13 * love.graphics.getHeight()}, size={ 0.18 *love.graphics.getWidth(), 0.67 * love.graphics.getHeight()}} then
            if state == "closed" then
                if inventory.isSelected("Locker key") then
                    state = "open"
                    inventory.removeItem("Locker key")
                else
                    messageManager.setMessage(game.storyboard.genericLocked)
                end
            elseif state == "open" then
                game.state.hasHazmat = true
                state = "openSuitless"
                timeout = love.timer.getTime()
            end
        end
    end
end
function game.draw()
    if state == "closed" or state == "closedSuitless" then
        love.graphics.draw(content.lockersClosed, game.shared.xOffset, game.shared.yOffset, 0,
            game.shared.scale,
            game.shared.scale)
    elseif state == "open" then
        if not content.lockersOpen then
            content.lockersOpen = love.graphics.newImage("res/levels/lockers_open.jpg")
        end
        love.graphics.draw(content.lockersOpen, game.shared.xOffset, game.shared.yOffset, 0,
            game.shared.scale,
            game.shared.scale)
    else
        if not content.lockersOpenSuitless then
            content.lockersOpenSuitless = love.graphics.newImage("res/levels/lockers_open_suitless.jpg")
        end
        love.graphics.draw(content.lockersOpenSuitless, game.shared.xOffset, game.shared.yOffset, 0,
            game.shared.scale,
            game.shared.scale)
    end

end
