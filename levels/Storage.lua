content = {}
function content.openHall()
    --local openDoor = love.audio.newSource("res/sounds/")
end
function game.load()
    game.state.levelState = "light"
    content.storage = love.graphics.newImage("res/levels/storage.jpg");
    game.shared.scale = math.min(love.graphics.getWidth() / content.storage:getWidth(), 
        love.graphics.getHeight() / content.storage:getHeight());
    game.shared.yOffset = math.abs(game.shared.scale * content.storage:getHeight() - love.graphics.getHeight()) / 2;
    game.shared.xOffset = math.abs(game.shared.scale * content.storage:getWidth() - love.graphics.getWidth()) / 2;
    fxManager.loadFx("vignette")

    game.shared.content.soundSwitchOff = love.audio.newSource("res/sounds/switchOff.ogg", "static")
    game.shared.content.soundSwitchOn = love.audio.newSource("res/sounds/switchOn.ogg", "static")

    content.lockerBtnPos = {0.66 * love.graphics.getWidth(), 0.45 * love.graphics.getHeight()}
    content.lockerBtnSize = {0.15 * love.graphics.getWidth(), 0.20 * love.graphics.getHeight()}

    content.switchBtnPos = {0.485 * love.graphics.getWidth(), 0.45 * love.graphics.getHeight()}
    content.switchBtnSize = {0.01 * love.graphics.getWidth(), 0.03 * love.graphics.getHeight()}

    content.boxesBtnPos = {0.85 * love.graphics.getWidth(), 0.53 * love.graphics.getHeight()}
    content.boxesBtnSize = {0.13 * love.graphics.getWidth(), 0.2 * love.graphics.getHeight()}

    content.doorBtnPos = {0.505 * love.graphics.getWidth(), 0.41 * love.graphics.getHeight()}
    content.doorBtnSize = {0.05 * love.graphics.getWidth(), 0.23 * love.graphics.getHeight()}

end
function game.update(dt)
    if game.state.levelState == "dark" then
        if love.timer.getTime() - game.state.timeoutStart > 1 then
            game.state.levelState = "light"
        game.shared.content.soundSwitchOff:play()
        end
    else
        if gui.Button{text="lockers", pos=content.lockerBtnPos, size=content.lockerBtnSize} then
            if not game.state.hasHazmat then
                game.switchLevelFade("Lockers")
            else
                messageManager.setMessage(game.storyboard.nothingThere)
            end
        end
        if gui.Button{text="switch", pos=content.switchBtnPos, size=content.switchBtnSize} then
            game.state.levelState = "dark"
            game.state.timeoutStart = love.timer.getTime()
            game.shared.content.soundSwitchOn:play()
        end
        if gui.Button{text="door", pos=content.doorBtnPos, size=content.doorBtnSize} then
            game.switchLevelFade("Hall", content.openHall)
        end
        if gui.Button{text="mirror", pos={0.06 * love.graphics.getWidth(), 0.36 * love.graphics.getHeight()}, size={ 0.25 *love.graphics.getWidth(), 0.2 * love.graphics.getHeight()}} then
            messageManager.setMessage(game.storyboard.mirror)
        end
        if gui.Button{text="boxes", pos=content.boxesBtnPos, size=content.boxesBtnSize} then
            messageManager.setMessage(game.storyboard.boxes)
        end
    end
end
function game.draw()
    if game.state.levelState == "light" then
        love.graphics.draw(content.storage, game.shared.xOffset, game.shared.yOffset, 0,
            game.shared.scale,
            game.shared.scale)
    else
        if not content.storageDark then
            content.storageDark = love.graphics.newImage("res/levels/storage_dark.jpg");
        end
        love.graphics.draw(content.storageDark, game.shared.xOffset, game.shared.yOffset, 0,
            game.shared.scale,
            game.shared.scale)
    end
end
