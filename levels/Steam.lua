content = {}
function game.load()
    content.steam = love.graphics.newImage("res/levels/steam.jpg");
    content.smokeParticle = love.graphics.newImage("res/sprites/particles/smoke.png");

    initSmoke()
end

function initSmoke()
    content.smokeSystem = love.graphics.newParticleSystem(content.smokeParticle, 50)
    content.smokeSystem:setDirection(math.pi+ 0.1)
    content.smokeSystem:setSpeed(380)
    content.smokeSystem:setLifetime(-1)
    content.smokeSystem:setParticleLife(1)
    content.smokeSystem:setEmissionRate(30)
    content.smokeSystem:setRotation(-5, 5, 0.5)
    content.smokeSystem:setSpin(-5, 5)
    content.smokeSystem:setColors(255, 255, 255, 0, 255, 255, 255, 70, 255, 255, 255, 90, 255, 255, 255, 100, 255, 255, 255, 0)
    content.smokeSystem:setSizes(0.03, 0.25, 0.3)
    content.smokeSystem:setSpread(0.07)
    content.smokeSystem:start()
    content.smokeSystem:update(5)
end
function game.update(dt)
    local _,_,_,alpha = love.graphics.getColor()
    alpha = alpha / 255
    content.smokeSystem:setColors(255, 255, 255, 0, 255, 255, 255, 70*alpha, 255, 255, 255, 90*alpha, 255, 255, 255, 100*alpha, 255, 255, 255, 0)
    content.smokeSystem:update(dt)
    if gui.Button{text="barrels", pos={0.27 * love.graphics.getWidth(), 0.41 * love.graphics.getHeight()}, size={0.15 * love.graphics.getWidth(), 0.27 * love.graphics.getHeight()}} then
        if game.state.flashlightTaken then
            messageManager.setMessage(game.storyboard.barrels)
        else
            inventory.addItem("flashlight")
            game.state.flashlightTaken = true
        end
    end
    if gui.Button{text="pipe-right", pos={0.88 * love.graphics.getWidth(), 0}, size={0.12 * love.graphics.getWidth(), love.graphics.getHeight()}} then
        messageManager.setMessage(game.storyboard.genericLocked)
    end
    if gui.Button{text="ladder", pos={0.67 * love.graphics.getWidth(), 80}, size={ 0.13 *love.graphics.getWidth(), 0.8 * love.graphics.getHeight()}} then
        if game.state.steamStopped then

        else
            messageManager.setMessage(game.storyboard.steam)
        end
    end
    if gui.Button{text="pipes-left", pos={0, 80}, size={ 0.17 *love.graphics.getWidth(), 0.58 * love.graphics.getHeight()}} then
        game.switchLevelFade("Pipes")
    end

    if gui.Button{text="bottom-leave", pos={0, 0.90 * love.graphics.getHeight()}, size={love.graphics.getWidth(), 0.10 * love.graphics.getHeight()}} then
        game.switchLevelFade("Hall")
    end
end
function game.draw()
    love.graphics.draw(content.steam, game.shared.xOffset, game.shared.yOffset, 0,
        game.shared.scale,
        game.shared.scale)
    love.graphics.draw(content.smokeSystem, 0.882 * love.graphics.getWidth(), 0.55 * love.graphics.getHeight())
    if not game.state.flashlightTaken then
        if not content.flashlight then
            content.flashlight = love.graphics.newImage("res/sprites/flashlight.png");
        end
        love.graphics.draw(content.flashlight, 0.315 * love.graphics.getWidth(),
            0.469 * love.graphics.getHeight(), 0,
            game.shared.scale,
            game.shared.scale)
    end
end
