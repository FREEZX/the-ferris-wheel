content = {}
function game.load()
end
function game.update()
    if gui.Button{text="left-leave", pos={0.26 * love.graphics.getWidth(), 0.25 * love.graphics.getHeight()}, size={0.13 * love.graphics.getWidth(), 0.6 * love.graphics.getHeight()}} then
        game.switchLevelFade("Observatory")
    end
    if gui.Button{text="right-leave", pos={0.78 * love.graphics.getWidth(), 80}, size={0.25 * love.graphics.getWidth(), love.graphics.getHeight()}} then
        game.switchLevelFade("Storage")
    end
    if gui.Button{text="bigDoor", pos={0.53 * love.graphics.getWidth(), 0.38 * love.graphics.getHeight()}, size={ 0.13 *love.graphics.getWidth(), 0.22 * love.graphics.getHeight()}} then
        if game.state.hallDoorOpened then
            game.switchLevelFade("Steam")
        else
            messageManager.setMessage(game.storyboard.genericLocked)
        end
    end
end
function game.draw()
    if not game.state.hallDoorOpened then
        if not content.hall then
            content.hall = love.graphics.newImage("res/levels/hall.jpg");
        end
        love.graphics.draw(content.hall, game.shared.xOffset, game.shared.yOffset, 0,
            game.shared.scale,
            game.shared.scale)
    else
        if not content.hallOpened then
            content.hallOpened = love.graphics.newImage("res/levels/hall_open.jpg");
        end
        love.graphics.draw(content.hallOpened, game.shared.xOffset, game.shared.yOffset, 0,
            game.shared.scale,
            game.shared.scale)
    end
end
