game = {}
game.shared = {}
game.shared.content = {}
game.state = {}
game.availableLevels = {}
game.currentLevel = nil

function love.load()
    love.filesystem.load("helpers/Math.lua")()
    love.filesystem.load("helpers/Tables.lua")()
    love.filesystem.load("helpers/Mouse.lua")()
    love.filesystem.load("lib/base/Framework.lua")()
    love.filesystem.load("lib/sounds/SoundManager.lua")()
    love.filesystem.load("lib/fx/FxManager.lua")()
    love.filesystem.load("classes/MessageManager.lua")()
    love.filesystem.load("classes/Inventory.lua")()
    love.filesystem.load("classes/InventoryItem.lua")()
    love.filesystem.load("res/values/storyboard-en.lua")()
    love.filesystem.load("res/values/items.lua")()
    gui = require "lib/Quickie"

    inventory.inventoryFont = love.graphics.newFont("res/fonts/redoctober.ttf", 12)

    local files = love.filesystem.enumerate("levels")
    for _,v in ipairs(files) do
        print(v);
        game.availableLevels[v] = "levels/" .. v
    end

    game.shared.mouse = love.graphics.newImage("res/sprites/mouse.png");

    game.screenModes = love.graphics.getModes()

    table.sort(game.screenModes, function(a, b) return a.width*a.height < b.width*b.height end)
    love.graphics.setMode(game.screenModes[#game.screenModes].width, game.screenModes[#game.screenModes].height, true)

    ----------------------------------------------
    --                                          --
    --   AUDIO INIT                             --
    --                                          --
    ----------------------------------------------
    soundManager.setAmbient("res/audio/ambient.ogg");
    soundManager.playAmbient();
    soundManager.setAmbientVolume(0.1);
    game.switchLevel("Storage")
end
function love.keypressed(key)
   if key == "escape" then
      love.event.push("quit")   -- actually causes the app to quit
   end
end
function love.update(dt)
    --io.write("x: " .. (love.mouse.getX() / love.graphics.getWidth()) .. " y: " .. (love.mouse.getY() / love.graphics.getHeight()) .. "\n")
    game.update(dt)
    fxManager.update(dt)
    messageManager.update(dt)
    inventory.update(dt)
    fadeUpdate(dt)
    
end
function fadeUpdate(dt)
    if game.fadingIn then
        game.alpha = game.alpha + dt * game.fadeSpeed
        if game.alpha > 255 then
            game.alpha = 255
            game.fadingIn = false
        end
    end
    if game.fadingOut then
        game.alpha = game.alpha - dt * game.fadeSpeed
        if game.alpha < 0 then
            game.alpha = 0
            game.fadingOut = false
            game.fadingIn = true
            if game.middleFunction then
                game.middleFunction()
            end
            game.loadLevel(game.levelPath)
        end
    end
end
function love.draw()
    love.graphics.setColor(255, 255, 255, game.alpha)
    game.draw()
    fxManager.draw()
    inventory.draw()
    messageManager.draw()
    if game.alpha == 255 then
        gui.core.draw()
    end
end
