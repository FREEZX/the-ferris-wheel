game.items = {
    lockerKey = InventoryItem.create("Locker key", love.graphics.newImage("res/sprites/items/key.png")),
    flashlight = InventoryItem.create("Flashlight", love.graphics.newImage("res/sprites/items/flashlight.png"))
}