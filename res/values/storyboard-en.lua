game.storyboard = {
    genericLocked = {
        "It's locked.",
        "Cannot open it without a key.",
    },
    mirror = {
        "The mirror is dirty.",
        "A mirror."
    },
    deadScientist = {
        "It's a dead scientist",
        "What happened here?"
    },
    nothingThere = {
        "Nothing to do there.",
        "I think i finished with that"
    },
    barrels = {
        "I don't think i can roll those",
        "Just some barrels"
    },
    steam = {
        "I'm not getting through that, it's too hot",
        "Nope."
    },
    boxes = "Boxes filled with dangerous materials."
}